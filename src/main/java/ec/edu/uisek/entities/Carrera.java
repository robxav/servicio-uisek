package ec.edu.uisek.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;


/**
 * The persistent class for the carrera database table.
 * 
 */
@Entity
@Table(name="carrera", catalog = "`trading-erp`", schema = "`Academico`")
@NamedQuery(name="Carrera.findAll", query="SELECT c FROM Carrera c")
public class Carrera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_carrera")
	private Integer idCarrera;

	private String descripcion;

	private String nombre;

	private Boolean vigente;

	//bi-directional many-to-one association to Facultad
	@ManyToOne
	@JoinColumn(name="id_facultad")
	private Facultad facultad;

	//bi-directional many-to-one association to CarreraMateria
	@OneToMany(mappedBy="carrera")
	@JsonBackReference
	private List<CarreraMateria> carreraMaterias;

	public Carrera() {
	}

	public Integer getIdCarrera() {
		return this.idCarrera;
	}

	public void setIdCarrera(Integer idCarrera) {
		this.idCarrera = idCarrera;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Boolean getVigente() {
		return this.vigente;
	}

	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}

	public Facultad getFacultad() {
		return this.facultad;
	}

	public void setFacultad(Facultad facultad) {
		this.facultad = facultad;
	}

	public List<CarreraMateria> getCarreraMaterias() {
		return this.carreraMaterias;
	}

	public void setCarreraMaterias(List<CarreraMateria> carreraMaterias) {
		this.carreraMaterias = carreraMaterias;
	}

	public CarreraMateria addCarreraMateria(CarreraMateria carreraMateria) {
		getCarreraMaterias().add(carreraMateria);
		carreraMateria.setCarrera(this);

		return carreraMateria;
	}

	public CarreraMateria removeCarreraMateria(CarreraMateria carreraMateria) {
		getCarreraMaterias().remove(carreraMateria);
		carreraMateria.setCarrera(null);

		return carreraMateria;
	}

}