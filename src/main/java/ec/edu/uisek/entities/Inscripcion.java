package ec.edu.uisek.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;


/**
 * The persistent class for the inscripcion database table.
 * 
 */
@Entity
@Table(name="inscripcion", catalog = "`trading-erp`", schema = "`Academico`")
@NamedQuery(name="Inscripcion.findAll", query="SELECT i FROM Inscripcion i")
public class Inscripcion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="id_carrera")
	private Integer idCarrera;
	
	@Column(name="id_pensum")
	private Integer idPensum;

	private Boolean vigente;

	//bi-directional many-to-one association to DetalleMatricula
	@OneToMany(mappedBy="inscripcion")
	@JsonBackReference
	private List<DetalleMatricula> detalleMatriculas;

	//bi-directional many-to-one association to Estudiante
	@ManyToOne
	@JoinColumn(name="id_estudiante")
	
	private Estudiante estudiante;

	public Inscripcion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdCarrera() {
		return this.idCarrera;
	}

	public void setIdCarrera(Integer idCarrera) {
		this.idCarrera = idCarrera;
	}

	public Boolean getVigente() {
		return this.vigente;
	}

	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}

	public List<DetalleMatricula> getDetalleMatriculas() {
		return this.detalleMatriculas;
	}

	public void setDetalleMatriculas(List<DetalleMatricula> detalleMatriculas) {
		this.detalleMatriculas = detalleMatriculas;
	}

	public DetalleMatricula addDetalleMatricula(DetalleMatricula detalleMatricula) {
		getDetalleMatriculas().add(detalleMatricula);
		detalleMatricula.setInscripcion(this);

		return detalleMatricula;
	}

	public DetalleMatricula removeDetalleMatricula(DetalleMatricula detalleMatricula) {
		getDetalleMatriculas().remove(detalleMatricula);
		detalleMatricula.setInscripcion(null);

		return detalleMatricula;
	}

	public Estudiante getEstudiante() {
		return this.estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	public Integer getIdPensum() {
		return idPensum;
	}

	public void setIdPensum(Integer idPensum) {
		this.idPensum = idPensum;
	}

	
}