package ec.edu.uisek.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Date;


/**
 * The persistent class for the detalle_matricula database table.
 * 
 */
@Entity
@Table(name="detalle_matricula", catalog = "`trading-erp`", schema = "`Academico`")

@NamedQuery(name="DetalleMatricula.findAll", query="SELECT d FROM DetalleMatricula d")
public class DetalleMatricula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_detalematricula")
	private Integer idDetalematricula;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private Boolean legalizado;

	//bi-directional many-to-one association to CarreraMateria
	@JoinColumn(name = "id_carrera_materia", referencedColumnName = "id_carrera_mnateria", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
   
	private CarreraMateria carreraMateria;
	
	//bi-directional many-to-one association to Inscripcion
	@JoinColumn(name = "id_matricula", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
   
	private Inscripcion inscripcion;

	public DetalleMatricula() {
	}

	public Integer getIdDetalematricula() {
		return this.idDetalematricula;
	}

	public void setIdDetalematricula(Integer idDetalematricula) {
		this.idDetalematricula = idDetalematricula;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Boolean getLegalizado() {
		return this.legalizado;
	}

	public void setLegalizado(Boolean legalizado) {
		this.legalizado = legalizado;
	}

	public CarreraMateria getCarreraMateria() {
		return this.carreraMateria;
	}

	public void setCarreraMateria(CarreraMateria carreraMateria) {
		this.carreraMateria = carreraMateria;
	}

	public Inscripcion getInscripcion() {
		return this.inscripcion;
	}

	public void setInscripcion(Inscripcion inscripcion) {
		this.inscripcion = inscripcion;
	}

}