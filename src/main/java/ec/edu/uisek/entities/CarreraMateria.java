package ec.edu.uisek.entities;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.List;


/**
 * The persistent class for the carrera_materias database table.
 * 
 */
@Entity
@Table(name="carrera_materias", catalog = "`trading-erp`", schema = "`Academico`")
@NamedQuery(name="CarreraMateria.findAll", query="SELECT c FROM CarreraMateria c")
public class CarreraMateria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_carrera_mnateria")
	private Integer idCarreraMnateria;

	private String aula;

	@Column(name="cupo_disponible")
	private Integer cupoDisponible;

	@Column(name="cupo_ocupado")
	private Integer cupoOcupado;

	@Column(name="cupo_total")
	private Integer cupoTotal;

	private String grupo;

	//bi-directional many-to-one association to Carrera
	@ManyToOne(optional = false)
	@JoinColumn(name="id_carrera")
   
	private Carrera carrera;

	//bi-directional many-to-one association to Horario
	@ManyToOne
	@JoinColumn(name="id_horario")
	
	private Horario horario;

	//bi-directional many-to-one association to DetalleMatricula
	@OneToMany(mappedBy="carreraMateria")
	@JsonBackReference
	private List<DetalleMatricula> detalleMatriculas;

	public CarreraMateria() {
	}

	public Integer getIdCarreraMnateria() {
		return this.idCarreraMnateria;
	}

	public void setIdCarreraMnateria(Integer idCarreraMnateria) {
		this.idCarreraMnateria = idCarreraMnateria;
	}

	public String getAula() {
		return this.aula;
	}

	public void setAula(String aula) {
		this.aula = aula;
	}

	public Integer getCupoDisponible() {
		return this.cupoDisponible;
	}

	public void setCupoDisponible(Integer cupoDisponible) {
		this.cupoDisponible = cupoDisponible;
	}

	public Integer getCupoOcupado() {
		return this.cupoOcupado;
	}

	public void setCupoOcupado(Integer cupoOcupado) {
		this.cupoOcupado = cupoOcupado;
	}

	public Integer getCupoTotal() {
		return this.cupoTotal;
	}

	public void setCupoTotal(Integer cupoTotal) {
		this.cupoTotal = cupoTotal;
	}

	public String getGrupo() {
		return this.grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public Carrera getCarrera() {
		return this.carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Horario getHorario() {
		return this.horario;
	}

	public void setHorario(Horario horario) {
		this.horario = horario;
	}

	public List<DetalleMatricula> getDetalleMatriculas() {
		return this.detalleMatriculas;
	}

	public void setDetalleMatriculas(List<DetalleMatricula> detalleMatriculas) {
		this.detalleMatriculas = detalleMatriculas;
	}

	public DetalleMatricula addDetalleMatricula(DetalleMatricula detalleMatricula) {
		getDetalleMatriculas().add(detalleMatricula);
		detalleMatricula.setCarreraMateria(this);

		return detalleMatricula;
	}

	public DetalleMatricula removeDetalleMatricula(DetalleMatricula detalleMatricula) {
		getDetalleMatriculas().remove(detalleMatricula);
		detalleMatricula.setCarreraMateria(null);

		return detalleMatricula;
	}

}