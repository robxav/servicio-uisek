package ec.edu.uisek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioUisekApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicioUisekApplication.class, args);
	}

}
