package ec.edu.uisek.controller;

import java.util.List;
import java.util.stream.Collectors;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.UsuarioService;
import ec.edu.uisek.entities.Usuario;


@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-usuario")
public class UsuarioController {
	
	
	@Autowired
	private UsuarioService usuarioSB;
	
	
	@GetMapping("/login/{email}/{password}")
	public Usuario loginUsuario(@PathVariable String email, @PathVariable String password) {

		Usuario user = new Usuario();
		try {
			user = usuarioSB.loginUsuario(email, password);
			return user;

		} catch (Exception e) {
			return null;
		}

	}
	
	@GetMapping("/ver/{id}")
	public Usuario detalle(@PathVariable int id) { // Para prueba de fallos /throws Exception
		Usuario user = usuarioSB.findById(id);
		return user;
	}
	
	
	@PostMapping("/crear")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario crear(@RequestBody Usuario user) {
		return usuarioSB.save(user);
	}
	
	@GetMapping("/listar")
	public List<ec.edu.uisek.entities.Usuario> listar() {
		return usuarioSB.findAll().stream().map(p -> {
			return p;
		}).collect(Collectors.toList());
	}
 
}
