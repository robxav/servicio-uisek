package ec.edu.uisek.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.CarreraMateriaService;
import ec.edu.uisek.entities.CarreraMateria;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-carmat")
public class CarreraMateriaController {

	@Autowired
	private CarreraMateriaService service;

	@GetMapping("/listar/{idcarrera}/{idpensum}")
	public List<CarreraMateria> listar(@PathVariable Integer idcarrera, @PathVariable Integer idpensum) {
		return service.findByMateriasByPeriodo(idcarrera, idpensum).stream().map(p -> {
			return p;
		}).collect(Collectors.toList());
	}

	@PutMapping("/editar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public CarreraMateria editar(@RequestBody CarreraMateria carrera, @PathVariable Integer id) {
		CarreraMateria carre = carrera;
		carre.setIdCarreraMnateria(id);
		return service.save(carre);
	}

}
