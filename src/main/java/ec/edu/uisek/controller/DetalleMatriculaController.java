package ec.edu.uisek.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.CarreraMateriaService;
import ec.edu.uisek.dao.DetalleMatriculaService;
import ec.edu.uisek.entities.CarreraMateria;
import ec.edu.uisek.entities.DetalleMatricula;
import ec.edu.uisek.entities.Usuario;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-detallematri")
public class DetalleMatriculaController {

	@Autowired
	private DetalleMatriculaService service;

	@GetMapping("/listar/{idcarrera}/{idpensum}/{idestudiante}")
	public List<DetalleMatricula> listar(@PathVariable Integer idcarrera, @PathVariable Integer idpensum,
			@PathVariable Integer idestudiante) {
		return service.findByMateriasByPeriodo(idcarrera, idpensum, idestudiante).stream().map(p -> {
			return p;
		}).collect(Collectors.toList());
	}

	@PutMapping("/editar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public DetalleMatricula editar(@RequestBody DetalleMatricula carrera, @PathVariable Integer id) {
		DetalleMatricula carre = carrera;
		carre.setIdDetalematricula(id);
		return service.save(carre);
	}

	@DeleteMapping("/eliminar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void eliminar(@PathVariable int id) {
		service.delete(id);
	}

	@PostMapping("/crear")
	@ResponseStatus(HttpStatus.CREATED)
	public DetalleMatricula crear(@RequestBody DetalleMatricula user) {
		return service.save(user);
	}

}
