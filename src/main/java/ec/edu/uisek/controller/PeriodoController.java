package ec.edu.uisek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.EstudianteService;
import ec.edu.uisek.dao.PeriodoService;
import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.entities.Periodo;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-periodo")
public class PeriodoController {
	
	@Autowired
	private PeriodoService periodoSB;

	@GetMapping("/find/{vigente}")
	public Periodo findPeriodoVigente(@PathVariable Boolean vigente) {

		Periodo periodo = new Periodo();
		try {
			periodo = periodoSB.findPeriodoVigente(vigente);
			return periodo;

		} catch (Exception e) {
			return null;
		}

	}

}
