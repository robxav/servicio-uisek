package ec.edu.uisek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.InscripcionService;
import ec.edu.uisek.dao.UsuarioService;
import ec.edu.uisek.entities.Inscripcion;
import ec.edu.uisek.entities.Usuario;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-inscripcion")
public class InscripcionController {

	@Autowired
	private InscripcionService inscripSB;

	@GetMapping("/find/{idEstudiante}/{idPeriodo}")
	public Inscripcion findInscripcionByEstudiante(@PathVariable Integer idEstudiante,
			@PathVariable Integer idPeriodo) {

		Inscripcion in = new Inscripcion();
		try {
			in = inscripSB.findByEstudiantePensum(idEstudiante, idPeriodo);
			return in;

		} catch (Exception e) {
			return null;
		}

	}

}
