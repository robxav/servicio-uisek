package ec.edu.uisek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.EstudianteService;
import ec.edu.uisek.entities.Estudiante;


@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-estudiante")
public class EstudianteController {

	@Autowired
	private EstudianteService estudiandteSB;

	@GetMapping("/find/{cedula}")
	public Estudiante findEstudianteByCedula(@PathVariable String cedula) {

		Estudiante estud = new Estudiante();
		try {
			estud = estudiandteSB.findByCedula(cedula);
			return estud;

		} catch (Exception e) {
			return null;
		}

	}

}
