package ec.edu.uisek.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.uisek.dao.CarreraService;
import ec.edu.uisek.dao.UsuarioService;
import ec.edu.uisek.entities.Carrera;
import ec.edu.uisek.entities.Usuario;

@RestController
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
		RequestMethod.OPTIONS })
@RequestMapping(value = "servicio-carrera")
public class CarreraController {
	@Autowired
	private CarreraService carreraSB;

	@GetMapping("/ver/{id}")
	public Carrera detalle(@PathVariable int id) { // Para prueba de fallos /throws Exception
		Carrera carr = carreraSB.findById(id);
		System.out.println(carr.getFacultad().getNombre());
		return carr;
	}

}
