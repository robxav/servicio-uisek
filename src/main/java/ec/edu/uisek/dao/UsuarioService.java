package ec.edu.uisek.dao;

import java.util.List;

import ec.edu.uisek.entities.Usuario;

public interface UsuarioService {

	List<Usuario> findAll();

	void delete(int id);

	Usuario findById(int id);

	Usuario save(Usuario usuario);

	Usuario loginUsuario(String email, String password);

}
