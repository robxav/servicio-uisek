package ec.edu.uisek.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.entities.Inscripcion;
import ec.edu.uisek.repository.EstudianteRepository;
import ec.edu.uisek.repository.IncripcionReporitory;

@Service
public class InscripcionServiceImplement implements InscripcionService{
	
	@Autowired
	private IncripcionReporitory inscripcionRB;

	@Override
	@Transactional(readOnly = true)
	public Inscripcion findByEstudiantePensum(Integer idEstudiante, Integer idPensum) {

		Inscripcion in = new Inscripcion();
		try {
			
			in = inscripcionRB.findInscripcionByPeriodo(idEstudiante, idPensum);
			
			return in;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
