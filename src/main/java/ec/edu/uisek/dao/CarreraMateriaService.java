package ec.edu.uisek.dao;

import java.util.List;

import ec.edu.uisek.entities.CarreraMateria;

public interface CarreraMateriaService {

	List<CarreraMateria> findByMateriasByPeriodo(Integer idCarrera, Integer idPensum);

	CarreraMateria save(CarreraMateria entidad);

}
