package ec.edu.uisek.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.Carrera;
import ec.edu.uisek.entities.Usuario;
import ec.edu.uisek.repository.CarreraRepository;
import ec.edu.uisek.repository.UsuarioRepository;

@Service
public class CarreraServiceImplement implements CarreraService{
	@Autowired
	private CarreraRepository carreraRep;
	
	
	
	@Override
	@Transactional(readOnly = true)
	public Carrera findById(int id) {
		return carreraRep.findById(id).orElse(null);
	}
	
	
	

}
