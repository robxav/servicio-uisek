package ec.edu.uisek.dao;

import ec.edu.uisek.entities.Inscripcion;

public interface InscripcionService {

	Inscripcion findByEstudiantePensum(Integer idEstudiante, Integer idPensum);

}
