package ec.edu.uisek.dao;

import ec.edu.uisek.entities.Periodo;

public interface PeriodoService {

	Periodo findPeriodoVigente(Boolean vigente);

}
