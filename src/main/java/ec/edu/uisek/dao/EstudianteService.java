package ec.edu.uisek.dao;

import ec.edu.uisek.entities.Estudiante;

public interface EstudianteService {

	Estudiante findByCedula(String cedula);

}
