package ec.edu.uisek.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.CarreraMateria;
import ec.edu.uisek.entities.DetalleMatricula;
import ec.edu.uisek.repository.DetalleMatriculaRepository;

@Service
public class DetalleMatriculaServiceImplement implements DetalleMatriculaService {

	@Autowired

	private DetalleMatriculaRepository detalleRP;

	@Override
	@Transactional(readOnly = true)
	public List<DetalleMatricula> findByMateriasByPeriodo(Integer idCarrera, Integer idPensum, Integer IdEstudiante) {

		List<DetalleMatricula> carreras = new ArrayList<DetalleMatricula>();
		try {
			carreras = detalleRP.findDetalleByMatriculaPeriodo(idCarrera, idPensum, IdEstudiante);

			return carreras;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	@Transactional
	public DetalleMatricula save(DetalleMatricula entidad) {
		return detalleRP.save(entidad);
	}
	
	@Override
	@Transactional
	public void delete(int id) {
		detalleRP.deleteById(id);
	}

}
