package ec.edu.uisek.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.CarreraMateria;
import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.repository.CarreraMateriaRepository;
import ec.edu.uisek.repository.CarreraRepository;
import ec.edu.uisek.repository.EstudianteRepository;


@Service
public class CarreraMateriaServiceImplement implements CarreraMateriaService {

	@Autowired
	private CarreraMateriaRepository carreraRP;

	@Override
	@Transactional(readOnly = true)
	public List<CarreraMateria> findByMateriasByPeriodo(Integer idCarrera, Integer idPensum) {

		List<CarreraMateria> carreras = new ArrayList<CarreraMateria>();
		try {
			carreras = carreraRP.findMateriasByCarrera(idCarrera, idPensum);

			return carreras;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}
	
	@Override
	@Transactional
	public CarreraMateria save(CarreraMateria entidad) {

		return carreraRP.save(entidad);
	}

}
