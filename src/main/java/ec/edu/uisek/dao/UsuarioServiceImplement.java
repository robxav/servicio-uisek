package ec.edu.uisek.dao;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.Usuario;
import ec.edu.uisek.repository.UsuarioRepository;

@Service
public class UsuarioServiceImplement implements UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRP;

	@Override
	@Transactional(readOnly = true)
	public Usuario loginUsuario(String email, String password) {
		String passwordBCrypt = DigestUtils.md5Hex(password);
		Usuario user = new Usuario();

		try {
			user = usuarioRP.loginUsuario(email, passwordBCrypt);

			return user;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	@Transactional
	public Usuario save(Usuario usuario) {

		try {
			String passwordBCrypt = DigestUtils.md5Hex(usuario.getClave());
			usuario.setClave(passwordBCrypt);
			return usuarioRP.save(usuario);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findById(int id) {
		return usuarioRP.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void delete(int id) {
		usuarioRP.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return (List<Usuario>) usuarioRP.findAll();
	}

}
