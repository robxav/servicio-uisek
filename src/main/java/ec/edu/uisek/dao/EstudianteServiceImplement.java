package ec.edu.uisek.dao;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.entities.Usuario;
import ec.edu.uisek.repository.EstudianteRepository;

@Service
public class EstudianteServiceImplement implements EstudianteService {

	@Autowired
	private EstudianteRepository estudianteRb;

	@Override
	@Transactional(readOnly = true)
	public Estudiante findByCedula(String cedula) {

		Estudiante estud = new Estudiante();
		try {
			estud = estudianteRb.findEstudianteByCedula(cedula);
			return estud;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
