package ec.edu.uisek.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.entities.Periodo;
import ec.edu.uisek.repository.EstudianteRepository;
import ec.edu.uisek.repository.PeriodoRepository;

@Service
public class PeriodoServiceImplement implements PeriodoService {

	@Autowired
	private PeriodoRepository periodoRB;

	@Override
	@Transactional(readOnly = true)
	public Periodo findPeriodoVigente(Boolean vigente) {

		Periodo period = new Periodo();
		try {
			period = periodoRB.findPeriodoVigente(vigente);

			return period;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
