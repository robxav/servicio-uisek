package ec.edu.uisek.dao;

import java.util.List;

import ec.edu.uisek.entities.DetalleMatricula;

public interface DetalleMatriculaService {

	List<DetalleMatricula> findByMateriasByPeriodo(Integer idCarrera, Integer idPensum, Integer IdEstudiante);

	DetalleMatricula save(DetalleMatricula entidad);

	void delete(int id);

}
