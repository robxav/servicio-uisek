package ec.edu.uisek.dao;

import ec.edu.uisek.entities.Carrera;

public interface CarreraService {

	Carrera findById(int id);

}
