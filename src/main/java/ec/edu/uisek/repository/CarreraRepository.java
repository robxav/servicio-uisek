package ec.edu.uisek.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.edu.uisek.entities.Carrera;

public interface CarreraRepository extends JpaRepository<Carrera, Integer>{

}
