package ec.edu.uisek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.uisek.entities.Usuario;


public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{
	@Query("select u from Usuario u where u.email=?1 and u.clave=?2")
	public Usuario loginUsuario(@Param("username") String username, @Param("password") String password);

}
