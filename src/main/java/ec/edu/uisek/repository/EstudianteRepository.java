package ec.edu.uisek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.entities.Usuario;

public interface EstudianteRepository extends  JpaRepository<Estudiante, Integer>{
	
	@Query("select u from Estudiante u where u.cedula=?1 ")
	public Estudiante findEstudianteByCedula(@Param("cedula") String cedula);

}
