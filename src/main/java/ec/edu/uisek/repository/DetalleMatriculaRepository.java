package ec.edu.uisek.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.uisek.entities.CarreraMateria;
import ec.edu.uisek.entities.DetalleMatricula;

public interface DetalleMatriculaRepository extends JpaRepository<DetalleMatricula, Integer>{
	
	@Query("select u from DetalleMatricula u where u.inscripcion.idCarrera=?1 and u.inscripcion.idPensum=?2 and u.inscripcion.estudiante.idEstudiante =?3")
	public List<DetalleMatricula> findDetalleByMatriculaPeriodo(@Param("idMatricula") Integer idcarrera, @Param("idPensum") Integer idPensum , @Param("idEstudiante") Integer idEstudiante);
	
}
