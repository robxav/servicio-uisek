package ec.edu.uisek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.uisek.entities.Inscripcion;
import ec.edu.uisek.entities.Usuario;

public interface IncripcionReporitory extends JpaRepository<Inscripcion, Integer>{
	
	@Query("select u from Inscripcion u where u.estudiante.idEstudiante=?1 and u.idPensum=?2")
	public Inscripcion findInscripcionByPeriodo(@Param("idEstudiante") Integer idEstudiante, @Param("idPensum") Integer idPensum);

}
