package ec.edu.uisek.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.uisek.entities.Estudiante;
import ec.edu.uisek.entities.Periodo;

public interface PeriodoRepository extends JpaRepository<Periodo, Integer>{
	
	
	@Query("select u from Periodo u where u.vigente =?1 ")
	public Periodo findPeriodoVigente(@Param("vigente") Boolean vigente);

}
