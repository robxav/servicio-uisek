package ec.edu.uisek.repository;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ec.edu.uisek.entities.CarreraMateria;
import ec.edu.uisek.entities.Usuario;

public interface CarreraMateriaRepository extends JpaRepository<CarreraMateria, Integer>{
	
	
	@Query("select u from CarreraMateria u where u.carrera.idCarrera=?1 and u.horario.idPensum=?2")
	public List<CarreraMateria> findMateriasByCarrera(@Param("idCarrera") Integer idcarrera, @Param("idPensum") Integer idPensum);

}
